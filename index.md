---
layout: default
title: Home
---

# My Personal Portfolio

Welcome to my website! My name is Jessica. 
I’m a UX/UI designer currently working at Shopify developing interfaces that users love.
I have a passion for all things design and accessibility, with a keen eye for making websites
that are functional and also user-friendly. I'm an expert in creating wireframes, mockups, and prototypes that
take ideas and turn them into reality. I hope you'll have a look around and use the links on the sidebar to reach out
with any questions or opportunities. 


[Resume](./assets/files/resume.pdf)
