---
layout: single
title: Work Experience
permalink: /workexperience/
---



**BMO - Ottawa, ON**
* position: Junior UX Designer
* duration: January 2019 - April 2019
* summary:  Create user interfaces for web and mobile

**TD Bank - Montreal, QC**
* position: UI/UX Lead
* duration: October 2019 - Present
* summary:  Lead UI/UX projects across the company


