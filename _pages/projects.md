---
layout: single
title: Projects
permalink: /projects/
---

## HealthMe
Worked with health care professional to explore and design the user experience for the HealthMe app.

* Interviewed users to gather requirements
* Drafted UX designs and presented them to other members
* Lead brainstorming and design sessions


#### Achievements
* Acheive 80% user satisfaction 3 months after launch\
* 65% user adoption after 6 months


## Go!Creative
Planned and directed a weekly volunteer group for university students wanting to learn UI/UX design.


